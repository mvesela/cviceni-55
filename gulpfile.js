// ==========================================
// TABLE OF CONTENT
// ==========================================
// 1. Dependencies
// 2. Config
// 3. Functions
// 4. Gulp tasks


// ==========================================
// 1. DEPENDENCIES
// ==========================================
// gulp-dev-dependencies
const {
  dest, lastRun, parallel, series, src, watch,
} = require('gulp');
// check package.json for gulp plugins
const gulpLoadPlugins = require('gulp-load-plugins');
// dev-dependencies
const browserSync = require('browser-sync').create();
const del = require('del');
const fs = require('fs');
const { rollup } = require('rollup');
const commonjs = require('@rollup/plugin-commonjs');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { babel } = require('@rollup/plugin-babel');
const { terser } = require('rollup-plugin-terser');
const postcssAutoprefixer = require('autoprefixer');
const postcssCssnano = require('cssnano');
const imageSize = require('image-size');
const escapeRegExp = require('lodash/escapeRegExp');

const typeset = require('./scripts/typeset');
const t = require('./src/translations');
const pkg = require('./package.json');

const $ = gulpLoadPlugins();
const { version } = pkg;

// ==========================================
// 2. CONFIG
// ==========================================
const config = {
  // COMMAND ARGUMENTS
  cmd: {
    // check if 'gulp --production'
    // http://stackoverflow.com/questions/28538918/pass-parameter-to-gulp-task#answer-32937333
    production: process.argv.indexOf('--production') > -1 || false,
    // get "--cviceni" argument value if present to build only one specific cviceni
    cviceni: process.argv.indexOf('--cviceni') > -1 ? process.argv[process.argv.indexOf('--cviceni') + 1] : false,
  },
  // FOLDERS
  src: {
    folder: 'src/',
    audio: {
      files: 'src/audio/**/*.{mp3,wav,ogg}',
    },
    data: {
      folder: 'src/data/',
      json: 'src/data/**/*.json',
      bundle: 'src/data/cviceni.json',
    },
    fonts: {
      folder: 'src/fonts/',
      files: 'src/fonts/**/*.*',
    },
    img: {
      folder: 'src/img/',
      files: 'src/img/**/*.{jpg,png,svg,gif}',
    },
    js: {
      app: 'src/js/cviceni/app.js',
      components: 'src/js/components/components.js',
      cviceni: 'src/js/cviceni/cviceni.js',
      files: 'src/js/**/*.js',
      library: 'src/js/lib/',
      vendor: [
        'node_modules/platform/platform.js',
        'node_modules/wavesurfer.js/dist/wavesurfer.min.js',
      ],
      vendorFiles: 'src/js/vendor/**/*.js',
      platform: 'node_modules/platform/platform.js',
    },
    pdf: {
      folder: 'src/pdf/',
      files: 'src/pdf/**/*.pdf',
    },
    pug: {
      views: 'src/views/**/*.pug',
      folder: 'src/views/',
      cviceni: 'src/views/cviceni/*/index.pug',
      cviceniFolder: 'src/views/cviceni/',
      partials: 'src/views/_partials/**/*.pug',
      partialsFolder: 'src/views/_partials/',
      saved: 'src/views/cviceni/saved.pug',
    },
    scss: 'src/scss/**/*.scss',
    text: {
      folder: 'src/text/',
      files: 'src/text/**/*.html',
    },
    video: {
      files: 'src/video/**/*.{mp4,webm}',
    },
    scaffolding: {
      folder: 'src/scaffolding/',
      data: {
        folder: 'src/scaffolding/data/',
      },
      views: {
        folder: 'src/scaffolding/views/',
        cviceni: {
          folder: 'src/scaffolding/views/cviceni/',
        },
      },
      text: {
        folder: 'src/scaffolding/text/',

      },
    },
  },
  tmp: {
    folder: 'tmp/',
    data: {
      folder: 'tmp/data/',
      cviceni: 'tmp/data/cviceni.json',
    },
    js: {
      folder: 'tmp/js/',
      src: 'tmp/js/**/*.js',
    },
    pug: {
      folder: 'tmp/pug/',
      index: 'tmp/pug/index.pug',
      src: 'tmp/pug/**/*.pug',
    },
  },
  dist: {
    folder: 'dist/',
    audio: 'dist/cviceni/assets/audio/',
    cviceni: 'dist/cviceni/',
    css: 'dist/cviceni/assets/css/',
    fonts: 'dist/cviceni/assets/fonts/',
    img: 'dist/cviceni/assets/img/',
    js: 'dist/cviceni/assets/js/',
    jsVendor: 'dist/cviceni/assets/js/vendor/',
    pdf: 'dist/cviceni/assets/pdf/',
    video: 'dist/cviceni/assets/video/',
  },
  // plugin settings
  // SERVER
  browserSync: {
    // proxy: 'localhost:' + config.port,
    // port: 3000,
    server: {
      baseDir: 'dist/cviceni/',
      directory: true,
    },
    files: null,
    // files: 'dist/**/*.*',
    ghostMode: {
      click: true,
      // location: true,
      forms: true,
      scroll: true,
    },
    injectChanges: true,
    logFileChanges: true,
    logLevel: 'info',
    notify: false,
    reloadDelay: 380,
    // startPath: '/cviceni/'
  },
  // IMAGES
  images: {},
  // PLUMBER
  plumber: {
    errorHandler: $.notify.onError('Error: <%= error.message %>'),
  },
  // POSTCSS
  postcss: {
    plugins: [
      postcssAutoprefixer(),
      postcssCssnano(),
    ],
  },
  // PUG
  pug: {
    locals: {
      escapeRegExp, // lodash.escapeRegExp
      imageSize, // get dimensions of images
      t, // translation
      typeset, // non-break spacing for widows & single characters
    },
  },
  // ROLLUP
  rollup: {
    d3: {
      bundle: {
        input: 'src/js/vendor/d3.js',
        plugins: [
          nodeResolve(),
          babel({
            // exclude: 'node_modules/**',
            babelHelpers: 'bundled',
          }),
          terser(),
        ],
        onwarn(warning, warn) {
          if (warning.code === 'CIRCULAR_DEPENDENCY') return;
          warn(warning);
        },
      },
      output: {
        file: 'dist/cviceni/assets/js/vendor/d3.build.js',
        format: 'iife',
        globals: {
          d3: 'd3',
        },
        name: 'd3',
        sourcemap: true,
      },
    },
    prosemirror: {
      bundle: {
        input: 'src/js/vendor/prosemirror.js',
        plugins: [
          nodeResolve(),
          babel({
            // exclude: 'node_modules/**',
            babelHelpers: 'bundled',
          }),
          terser(),
        ],
        onwarn(warning, warn) {
          if (warning.code === 'CIRCULAR_DEPENDENCY') return;
          warn(warning);
        },
      },
      output: {
        file: 'dist/cviceni/assets/js/vendor/prosemirror.build.js',
        format: 'iife',
        globals: {
          prosemirror: 'pm',
        },
        name: 'pm',
        sourcemap: true,
      },
    },
    cviceni: {
      bundle: {
        input: 'src/js/cviceni/app.js',
        plugins: [
          commonjs(),
          nodeResolve(),
          babel({
            // exclude: 'node_modules/**',
            babelHelpers: 'bundled',
          }),
          terser(),
        ],
        onwarn(warning, warn) {
          if (warning.code === 'CIRCULAR_DEPENDENCY') return;
          warn(warning);
        },
      },
      output: {
        file: 'dist/cviceni/assets/js/app.build.js',
        format: 'iife',
        name: 'cviceni',
        sourcemap: true,
      },
    },
    components: {
      bundle: {
        input: 'src/js/components/components.js',
        plugins: [
          commonjs(),
          nodeResolve(),
          babel({
            // exclude: 'node_modules/**',
            babelHelpers: 'bundled',
          }),
          terser(),
        ],
        onwarn(warning, warn) {
          if (warning.code === 'CIRCULAR_DEPENDENCY') return;
          warn(warning);
        },
      },
      output: {
        file: 'dist/cviceni/assets/js/components.build.js',
        format: 'iife',
        name: 'components',
        sourcemap: true,
      },
    },
  },
  // SASS
  sass: {
    errLogToConsole: true,
    outputStyle: 'expanded',
  },
};

// ==========================================
// 3. FUNCTIONS
// ==========================================
function serve(done) {
  if (browserSync.active) {
    return;
  }
  browserSync.init(config.browserSync);
  done();
}

function reload(done) {
  browserSync.reload();
  done();
}

function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

function getAudioTextContent(cviceni, item) {
  if (Object.prototype.hasOwnProperty.call(item, 'prepis')) {
    try {
      item.content = fs.readFileSync(`${config.src.text.folder + cviceni}/${item.prepis}.html`, 'utf8');
    }
    catch (err) {
      throw new Error(`could not load a content html file for an editor${err}`);
    }
  }
}

function injectTextsForModules(dataFiles) {
  Object.keys(dataFiles).forEach((file) => {
    const cviceni = dataFiles[file].cviceni.slug;
    const slajdy = dataFiles[file].slajd;

    slajdy.forEach((slajd) => {
      // 1. Textový editor
      // - slajd.textovyEditor.texty[].text
      if (Object.prototype.hasOwnProperty.call(slajd, 'textovyEditor')) {
        slajd.textovyEditor.texty.forEach((text) => {
          try {
            text.content = fs.readFileSync(`${config.src.text.folder + cviceni}/${text.text}.html`, 'utf8');
          }
          catch (err) {
            throw new Error(`could not load a content html file for an editor${err}`);
          }
        });
      }

      // 2. Přepisy audio & video
      // - slajd.media.soubory[].prepis
      // - slajd.TOOL.galerie[].prepis, all tools which can have gallery:
      const toolsWithGallery = ['klicovaSlova', 'media', 'razeni', 'svg', 'testKviz', 'textovyEditor', 'uzivatelskyText', 'vyber'];

      toolsWithGallery.forEach((toolWithGallery) => {
        if (slajd[toolWithGallery] && slajd[toolWithGallery].galerie) {
          slajd[toolWithGallery].galerie.forEach((item) => getAudioTextContent(cviceni, item));
        }
      });

      if (Object.prototype.hasOwnProperty.call(slajd, 'media')) {
        slajd.media.soubory.forEach((item) => getAudioTextContent(cviceni, item));
      }

    });
  });

  return dataFiles;
}

// create JSON data file name from --slug to the camelCase:
// 'name-of-cviceni' –> 'cviceniNameOfCviceni'
function createDataFileName(str) {
  const stringArray = str.split('-');

  stringArray.forEach((part, index) => {
    stringArray[index] = upperCaseFirst(part);
  });

  return `cviceni${stringArray.join('')}`;
}


// ==========================================
// 4. TASKS
// ==========================================
// CLEAN
function clean() {
  return del([config.dist.folder, config.tmp.folder]);
}

// DATA
function data() {
  const dataFiles = config.cmd.cviceni ? `${config.src.data.folder}${createDataFileName(config.cmd.cviceni)}.json` : config.src.data.json;

  return src([dataFiles])
    .pipe($.plumber(config.plumber))
    // .pipe($.debug({
    //   title: 'data-source:',
    // }))
    .pipe($.jsoncombine('cviceni.json', (dataFiles) => Buffer.from(JSON.stringify(injectTextsForModules(dataFiles, config)))))
    .pipe($.template({ version }))
    .pipe(dest(config.tmp.data.folder))
  // .pipe($.debug({ title: 'data-result:' }));
}

const exercise = `${config.src.pug.cviceniFolder + config.cmd.cviceni}/index.pug`;
// CVICENI (pug -> html)
function pug() {
  return src(config.cmd.cviceni ? exercise : config.src.pug.cviceni)
    .pipe($.plumber(config.plumber))
    .pipe($.data((file) => JSON.parse(
      fs.readFileSync(config.tmp.data.cviceni),
    )))
    .pipe($.pug(config.pug))
    .pipe(dest(config.cmd.cviceni ? config.dist.cviceni + config.cmd.cviceni : config.dist.cviceni));
}

// SASS
function css() {
  return src(config.src.scss, {
    sourcemaps: true,
  })
    .pipe($.sass(config.sass).on('error', $.sass.logError))
    .pipe($.if(config.cmd.production, $.postcss(config.postcss.plugins)))
    .pipe(dest(config.dist.css, { sourcemaps: './maps' }))
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

// JAVASCRIPT
async function jsComponents() {
  const bundle = await rollup(config.rollup.components.bundle);
  await bundle.write(config.rollup.components.output);
}

async function jsCviceni() {
  const bundle = await rollup(config.rollup.cviceni.bundle);
  await bundle.write(config.rollup.cviceni.output);
}

// D3.js — D3 is a JavaScript library for visualizing data with HTML, SVG, and CSS.
async function jsD3() {
  const bundle = await rollup(config.rollup.d3.bundle);
  await bundle.write(config.rollup.d3.output);
}

// PROSEMIRROR editor tool
async function jsProsemirror() {
  const bundle = await rollup(config.rollup.prosemirror.bundle);
  await bundle.write(config.rollup.prosemirror.output);
}

// const js = parallel(jsComponents, jsCviceni, jsD3);

// JAVASCRIPT - VENDOR imported from node_modules
function jsVendorNodeModules() {
  return src(config.src.js.vendor)
    .pipe(dest(config.dist.jsVendor));
}

// IMAGES
function images() {
  return src(config.src.img.files, {
    since: lastRun(images),
  })
    .pipe($.if(config.cmd.production, $.imagemin(config.images)))
    .pipe(dest(config.dist.img))
    .pipe($.if(config.cmd.production, $.responsive({
      '**/*.{jpg,png}': [
        {
          width: 480,
          quality: 60,
          rename: {
            suffix: '-thumb',
          },
        },
      ],
    },
    {
      errorOnUnusedImage: false,
      errorOnEnlargement: false,
      silent: true, // config.cmd.production,
      withMetadata: false,
    })))
    .pipe($.if(config.cmd.production, $.imagemin(config.images)))
    .pipe(dest(config.dist.img));
}

// AUDIO
function audio() {
  return src(config.src.audio.files, {
    since: lastRun(audio),
  })
    .pipe(dest(config.dist.audio));
}
// VIDEO
function video() {
  return src(config.src.video.files, {
    since: lastRun(video),
  })
    .pipe(dest(config.dist.video));
}

// PDF
function pdf() {
  return src(config.src.pdf.files, {
    since: lastRun(pdf),
  })
    .pipe(dest(config.dist.pdf));
}

// FONTS
function fonts() {
  return src(config.src.fonts.files, {
    since: lastRun(fonts),
  })
    .pipe(dest(config.dist.fonts));
}

// WATCH
function watcher(done) {
  // data
  watch([config.src.data.json, config.src.text.files], series(data, pug, reload));
  // pug (build all)
  watch(config.src.pug.views, series(pug, reload));
  // css
  watch(config.src.scss, series(css));
  // js:app
  watch([config.src.js.files, `!${config.src.js.vendorFiles}`], series(parallel(jsComponents, jsCviceni), reload));
  // js:vendor
  watch(config.src.js.vendorFiles, series(parallel(jsD3, jsProsemirror), reload));
  // images
  watch(config.src.img.files, series(images, reload));
  // audio
  watch(config.src.audio.files, series(audio, reload));
  // video
  watch(config.src.video.files, series(video, reload));
  // fonts
  watch(config.src.fonts.folder, series(fonts, reload));

  done();
}

// GULP
exports.data = series(data);

exports.js = parallel(
  jsComponents,
  jsCviceni,
  jsD3,
  jsVendorNodeModules,
  jsProsemirror,
);

exports.default = series(
  clean,
  parallel(
    series(data, pug),
    css,
    jsComponents, jsCviceni,
    jsD3, jsProsemirror, jsVendorNodeModules,
    fonts,
    images,
    audio,
    video,
  ),
  serve,
  watcher,
);

exports.build = series(
  clean,
  parallel(
    series(data, pug),
    css,
    jsComponents, jsCviceni,
    jsD3, jsProsemirror, jsVendorNodeModules,
    fonts,
    images,
    audio,
    video,
    pdf,
  ),
);
